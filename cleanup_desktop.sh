#!/bin/bash

# Shell script to sort files into their respective directories
# Like .mp3 to => Desktop/mp3/ etc.

DIR=$HOME'/Desktop/'
for file in $DIR*
do
    if [[ -f $file ]]; then
    	# Get file extension
    	extension="${file##*.}"
    	# Create directory if it is not already there
    	mkdir -p $DIR/$extension
    	# Move file to respective folder and also display file being moved (using -v | verbose flag )
    	mv -v $file $DIR$extension/ 
	fi
done

# End of script