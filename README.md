# display_largest_files.sh 
	Shell script to print top 10 largest file in home/*
	It uses find to do so.

Usage:

`bash display_largest_files.sh`

# cleanup_desktop.sh
	Shell script to sort files into their respective directories
	Like .mp3 to => Desktop/mp3/ , .mp4 to => Desktop/mp4/ etc.
	
Usage:

`bash cleanup_desktop.sh`
