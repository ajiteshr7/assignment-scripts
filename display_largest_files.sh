#!/bin/bash

# Shell script to print top 10 largest file in home/*

echo "FILE SIZE" \\t "FILE NAME"
# Here O3 for optimisation of command | sort in reverse and by numberic values | show top ten
find -O3 /home/ -size +1000k -type f -printf '%b %f\n' | sort -rn | head -10

# End of script